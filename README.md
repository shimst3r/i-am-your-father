# i_am_your_father

I Am Your Father ruins all the fun in Star Wars

![A screenshot taken from the app. It shows Darth Vader and a text saying "Luke, I am your father"](screenshot.png)

## Background

This is my first [Flutter](https://flutter.dev) project, implemented as one of the challenges during [The Complete 2020 Flutter Development Bootcamp with Dart](https://www.udemy.com/course/flutter-bootcamp-with-dart/).

## Credits

The font I used is [Star Jedi](https://www.dafont.com/de/star-jedi.font). The Darth Vader icon is taken from [icons8](https://www.icons8.com).
