import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xff25353D),
            title: Text(
              'luke, i am your father',
              style: TextStyle(fontFamily: 'Star Jedi'),
            ),
          ),
          backgroundColor: Color(0xffC2CAEA),
          body: Center(
            child: Image(
              image: AssetImage('images/darth-vader.png'),
            ),
          ),
        ),
      ),
    );
